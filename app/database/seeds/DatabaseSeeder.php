<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$user = new User();
		$user->firstname = 'Jon';
		$user->lastname = 'Doe';
		$user->email = 'admin@admin.com';
		$user->password = Hash::make('admin');
		$user->telephone = '0724 485 758';
		$user->admin = 1;
		$user->save();
		
	}

}

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		 $this->call('UserTableSeeder');
	}

}

