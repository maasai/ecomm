<?php
class ProductsController extends BaseController{

	public function __construct(){
		parent::__construct();
		$this->beforeFilter('csrf', array('on' => 'post'));
		$this->beforeFilter('admin');
	}
	public function getIndex(){
		$categories = array();
		foreach (Category::all() as $category) {
			$categories[$category->id] = $category->name;
		}

		$products = Product::all();
		$products = Product::paginate(3);

		return View::make('products.index')
		->with('products', $products)
		->with('categories', $categories);
	}

	public function postCreate(){
		$validator = Validator::make(Input::all(), Product::$rules);

		if($validator->passes()){
			$product = new Product;
			$product->category_id = Input::get('category_id');
			$product->title = Input::get('title');
			$product->description = Input::get('description');
			$product->price = Input::get('price');

			$image = Input::file('image');
			$filename = time().'-'.$image->getClientOriginalName();
			//$filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
			$path = 'assets/img/products/';
			//Image::make($image->getRealPath())->resize(468, 249)->save($path);

			$img = Image::make($image->getRealPath())->resize(468, 249)->save($path.'/'.$filename);

 			$product->image = 'assets/img/products/'.$filename;
			$product->save();

			return Redirect::to('admin/products/index')
			->with('message', 'Product Created.');
		}
		return Redirect::to('admin/products/index')
		->withErrors($validator)
		->withInput()
		->with('message', 'Something went wrong.');
	}

	public function postDestroy(){
		$product = Product::find(Input::get('id'));
		if($product){
			//delete the product image
			File::delete('public/'.$product->image);
			//delete the product
			$product->delete();
			return Redirect::to('admin/products/index')
			->with('message', 'Product Deleted Successfully.');
		}
		return Redirect::to('admin/products/index')
		->with('message', 'Product Not Found. Please Retry.');
	}

	public function postToggleAvailability(){
		$product = Product::find(Input::get('id'));

		if($product){
			$product->availability = Input::get('availability');
			$product->save();
			return Redirect::to('admin/products/index')
			->with('message', 'Product Updated.');
		}
		return Redirect::to('admin/products/index')
		->with('message', 'Invalid Product.');

	}
}