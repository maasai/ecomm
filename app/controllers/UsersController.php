<?php
class UsersController extends BaseController{

	public function __construct(){
		parent::__construct();
		$this->beforeFilter('csrf', array('on' => 'post'));
	}
//Register new users
	public function getNewaccount(){
		return View::make('users.newaccount');
	}

	//save the created users
	public function postCreate(){
		$validator = Validator::make(Input::all(), User::$rules);
		if($validator->passes()){
			$user = new User();
			$user->firstname = Input::get('firstname');
			$user->lastname = Input::get('lastname');
			$user->email = Input::get('email');
			$user->telephone = Input::get('telephone');
			$user->password = Hash::make(Input::get('password'));
			$user->save();
			return Redirect::to('users/signin')
			->with('message', 'Thank you for registratering. You may sign in.');
		}
		return Redirect::to('users/newaccount')
		->withErrors($validator)
		->withInput()
		->with('message', 'Something went wrong.');
	}
	public function getSignin(){
		return View::make('users.signin');
	}

	public function postSignin(){
		$credentials = array(
				'email'=> Input::get('email'),
				'password' => Input::get('password')
			);
		if(Auth::attempt($credentials)){
			return Redirect::to('/')->with('message', 'Thank you for signing in');
		}else{
			return Redirect::to('users/signin')->with('message', 'Your Email / Password combo was incorrect');
		}
	}

	public function getLogout(){
		Auth::logout();
		return Redirect::to('users/signin')->with('message', 'You have been signed out');
	}


	
}