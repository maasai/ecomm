@extends('layouts.main')
@section('content')
	<div id="new-account">
                    <h1>Create New Account</h1>
		@if($errors->has())
			<div id="form-errors">
			<p>The following errors have occured</p>
			<ul>
				@foreach ($errors->all() as $error) 
					<li>
					{{ $error }}
					</li>

				@endforeach
			</ul>
			</div> <!-- End Form-Errors -->
		@endif
                    {{Form::open(array('url' => 'users/create'))}}
                        <p>
                            {{ Form::label('firstname') }}
                            {{ Form::text('firstname', '', array('placeholder' => 'First Name')) }}
                        </p>
                          <p>
                            {{ Form::label('lastname') }}
                            {{ Form::text('lastname', '', array('placeholder' => 'Last Name')) }}
                        </p>
                          <p>
                            {{ Form::label('email') }}
                            {{ Form::email('email', '', array('placeholder' => 'Email Address')) }}
                        </p>
                          <p>
                            {{ Form::label('password') }}
                            {{ Form::password('password', '', array('placeholder' => 'Password')) }}
                        </p>
                          <p>
                            {{ Form::label('password_confirmation') }}
                            {{ Form::text('password_confirmation', '', array('placeholder' => 'Confirm Password')) }}
                        </p>
                          <p>
                            {{ Form::label('telephone') }}
                            {{ Form::text('telephone', '', array('placeholder' => 'Telephone')) }}
                        </p>
                        <hr />
                        {{Form::submit('CREATE NEW ACCOUNT', array('class' => 'secondary-cart-btn'))}}
                        {{Form::close()}}
	
                 		
                </div><!-- end new-account -->
@stop