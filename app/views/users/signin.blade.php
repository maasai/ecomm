@extends('layouts.main')
@section('content')
	<section id="signin-form">
                <h1>I have an account</h1>
                @foreach ($errors->all() as $error) 
					<li>
					{{ $error }}
					</li>

				@endforeach

				{{Form::open(array('url' => 'users/signin'))}}
                    <p>
                    	{{ HTML::image('assets/img/email.gif', 'Email Address')}}
                    	{{ Form::email('email', '', array('placeholder' => 'Email Address')) }}
                    </p>
                     <p>
                    	{{ HTML::image('assets/img/password.gif', 'Password')}}
                    	{{ Form::password('password', array('placeholder' => 'Password')) }}
                    </p>
                    {{ Form::submit('SIGN IN', array('class' => 'secondary-cart-btn')) }}
                    {{ Form::close() }}
            </section><!-- end signin-form -->

              <section id="signup">
                <h2>I'm a new customer</h2>
                <h3>You can create an account in just a few simple steps.<br>
                    Click below to begin.</h3>

                <a href="{{ URL::to('users/newaccount') }}" class="default-btn">CREATE NEW ACCOUNT</a>
            </section><!--- end signup -->
@stop